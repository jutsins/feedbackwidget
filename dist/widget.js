function widget(message, succes) {
    this.message = message;
    this.succes = succes;
    this.backgroundColor = this.succes ? "green" : "red";
}

module.exports = widget;
