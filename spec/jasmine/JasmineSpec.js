describe("Widget", function () {
    var widget = require('../../src/widget');
    var testWidget;

    //Testcase to check if the background colour is set upon creating a new widget.
    describe("When a widget is created", function () {
        it("should be green if the widget's succes is true", function () {
            testWidget = new widget("message", true);
            expect(testWidget.backgroundColor).toBe("green");
        });
        it("should be red if the widget's succes is false", function () {
            testWidget = new widget("message", false);
            expect(testWidget.backgroundColor).toBe("red");
        });
    });


});


