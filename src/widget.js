function widget(message, success) {
    this.message = message;
    this.succes = success;
    this.backgroundColor = this.succes ? "green" : "red";
}

module.exports = widget;

