const gulp = require('gulp');
const {src, dest} = require('gulp');

const autoPrefixer = require('gulp-autoprefixer');
const cleanCss = require('gulp-clean-css');


gulp.task('build', function(done){
   css();
   html();
   js();
   done();
});

const css = function () {
   return src(['*.css', 'src/*.css'])
       .pipe(autoPrefixer())
       .pipe(cleanCss())
       .pipe(dest("dist"));

};

const html = function(){
   return src(['*.html', 'src/*.html'])
       .pipe(dest("dist"));
};

const js = function(){
   return src('src/*.js')
       .pipe(dest('dist'));
}